# NEOPHASER; or, audio effects as applied to the retina

neophaser is a project designed to apply audio effect chains to image and video data. There is no better explanation than to use it yourself. Check out cool examples at https://neophaser.library.8oc.org.

Install with `pip install neophaser`, run with `python -m neophaser`.
